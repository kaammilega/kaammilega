<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$app->get('/', function () use ($app) {
	// echo "Hello ";
	// $user = new App\User;
	// $users = $user::all();
	// echo $users[0]->name;
	return view('index');
});

/**
 * Routes for resource admin_-panel
 */

$app->get('index',function()
{
	return view('index');
});
$app->get('admin',function()
{
	return view('login');
});
$app->get('home', function()
{
	return view('home');
});


$app->get('all_retailer',function()
{
	return view('all_retailer');
});

$app->get('all_retailer_job',function()
{
	return view('all_retailer_job');
});


$app->get('all_salesman',function()
{
	return view('all_salesman');
});

$app->get('all_salesman_job',function()
{
	return view('all_salesman_job');
});


$app->get('page_not_found',function()
{
	return view('page_404');
});
$app->get('unauthorized_access',function()
{
	return view('page_403');
});


$app->get('adding_department',function()
{
	return view('adding_department');
});

$app->get('adding_jobtitle',function()
{
	return view('adding_jobtitle');
});

$app->get('return_xml','Controller@return_xml');
//-----------------------------siddarth---------------------------------------------

$app->get('demo/','Controller@demo');
$app->get('demo/page/{pageNumber}/show/{showThis}','Controller@demoPagination');
$app->get('/getData','Controller@sendData');

// admin panel----------------------------------------------------------------------
$app->get('adminpanel', 'Admin_PanelsController@all');
$app->post('admin_check', 'Admin_PanelsController@admin_check');

$app->get('depart_check','Admin_PanelsController@department_check');

$app->post('allretailerjobs','Admin_PanelsController@allretailerjobs');
$app->post('all_retailer_data', 'Admin_PanelsController@allRetailer');

$app->post('allsalesmanjobs','Admin_PanelsController@allsalesmanjobs');
$app->post('all_salesman_data', 'Admin_PanelsController@allSalesman');

$app->get('allhired','Admin_PanelsController@allhired');

$app->get('premium_salesman','Admin_PanelsController@premium_salesman');

$app->get('datatable_jobtitles','JobtitleController@datatable_jobtitles');
$app->get('datatable_department','DepartmentController@datatable_department');
$app->group(['prefix' => 'api'], function($app)
{

$app->get('all-department','DepartmentController@all_department');
$app->post('add-department','DepartmentController@add_department');
$app->post('remove-department','DepartmentController@remove_department');
$app->post('rename-department','DepartmentController@rename_department');

$app->get('all-job-titles','JobtitleController@all_jobstitles');
$app->post('add-job-title','JobtitleController@add_jobtitle');
$app->post('remove-job-title','JobtitleController@remove_jobtitle');
$app->post('rename-job-title','JobtitleController@rename_jobtitle');


/**
 * Routes for resource user
 */
$app->get('user-salesman', 'UsersController@allSalesman');
$app->get('user-retailer', 'UsersController@allRetailer');
$app->get('user/{id}', 'UsersController@get');
$app->post('user', 'UsersController@add');
$app->put('user/{id}', 'UsersController@put');
$app->delete('user/{id}', 'UsersController@remove');
$app->post('update-user', 'UsersController@update_user');




/**
 * Routes for resource shop
 */
$app->get('shop', 'ShopsController@all');
$app->get('shop/{id}', 'ShopsController@get');
$app->post('shop', 'ShopsController@add');
//$app->put('shop/{id}', 'ShopsController@put');
$app->delete('shop/{id}', 'ShopsController@remove');
$app->post('shop-update', 'ShopsController@update');





/**
 * Routes for resource job
 */
$app->get('job', 'JobsController@all');
$app->get('job/{id}', 'JobsController@get');
$app->post('job', 'JobsController@add');
$app->post('job-update', 'JobsController@update');
$app->delete('job/{id}', 'JobsController@remove');



$app->post('add-experience','ExperienceController@create');
$app->get('salesman-experience/{sId}','ExperienceController@salesman_view');
$app->get('experience/{eId}','ExperienceController@view');
$app->post('update-experience','ExperienceController@update');




// $app->get('applicant/{aId}','ApplicantController@getApplicant');


$app->group(['prefix' => 'ui'], function($ui)
{
	$ui->get('premium_jobs','Admin_PanelsController@premium_jobs');

	$ui->get('get-shop-names', 'ShopsController@shopnames');

	$ui->get('all-new-jobs/{uId}','JobsController@all_newjobs');

	$ui->get('all-match-jobs/{uId}','JobsController@all_matchjobs');
	$ui->get('all-other-jobs/{uId}','JobsController@all_otherjobs');
	
	$ui->post('login', 'LoginsController@authenticate');
	$ui->post('verify-mail', 'LoginsController@verifymail');

	$ui->get('shop-images/{sId}', 'ImagesController@shopImages');
	$ui->post('delete-shop-image', 'ImagesController@deleteShopImage');
	$ui->post('delete-salesman-image', 'ImagesController@deleteSalesmanImage');
	$ui->post('save-salesman-image', 'ImagesController@saveSalesmanImage');
	$ui->post('add-shop-image', 'ImagesController@addShopImage');
	$ui->post('upload-salesman-image', 'ImagesController@uploadSalesmanImage');
	$ui->post('upload-shop-image', 'ImagesController@uploadShopImage');

	$ui->get('retailer-shops/{rId}', 'ShopsController@retailerShops');

	$ui->get('shop-jobs/{sId}', 'JobsController@shopJobs');

	$ui->post( 'add-applicants' , 'ApplicantController@add' );
	$ui->get('get-applicant-by-retailer/{rId}' , 'ApplicantController@getByRetailer' );
	$ui->post('applicant-status-change','ApplicantController@changeStatus');
	$ui->post('applicant-status-check','ApplicantController@checkStatus');
	$ui->get('applicants-shopid/{sId}', 'ApplicantController@allShopId');
	$ui->get('applicants-jobid/{jId}', 'ApplicantController@allJobId');
	$ui->post('applicant-detail', 'ApplicantController@getApplicantDetail');

	$ui->get('salesman-applied-jobs/{sId}','ApplicantController@getAppliedJobs');

	$ui->post('job-status-change','JobsController@statusUpdate');

	

});

$app->get('report','ReportsController@updateReport');
$app->get('get-report','ReportsController@all');
$app->get('stats','ReportsController@stats');
$app->post('sendStats','ReportsController@sendStats');
$app->post('check','ReportsController@check');

});

