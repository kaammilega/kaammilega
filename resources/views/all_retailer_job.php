<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Kaammilega | </title>

  
  <!-- Bootstrap -->
  <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- iCheck -->
  <link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

  <!-- bootstrap-progressbar -->
  <link href="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">  

  <!-- Custom Theme Style -->
  <link href="/build/css/custom.min.css" rel="stylesheet">
  <link href="/css_custom/css_custom_1.css" rel="stylesheet">
  <script>
    
    (function (){
      
     var result = sessionStorage.getItem("checkk");

     if( result != "admin")
     {
      location.href="unauthorized_access";
      
    }
    
  })();
</script>


</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="home" class="site_title"><div class="profile_pic"><img src="/production_files/images/56.png"/></div> <span>Kaammilega!</span></a>
          </div>

          <div class="clearfix"></div>

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <h3>Dashboard</h3>
              <ul class="nav side-menu">
                <li><a href="home"><i class="fa fa-bar-home"></i> Home </a></li>
                
                <li><a><i class="fa fa-home"></i> Adding <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                   <li><a href="adding_department"> Add Department </a></li>
                   <li><a href="adding_jobtitle"> Add Job Title </a></li>
                 </ul>
               </li>
               
               <li><a><i class="fa fa-home"></i> Retailers <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li><a href="all_retailer"> All Retailers </a></li>
                  <li><a href="all_retailer_job"> Retailers Posting </a></li>
                </ul>
              </li>
              <li><a><i class="fa fa-edit"></i> Salesmans <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li><a href="all_salesman"> All Salesmans </a></li>
                  <li><a href="all_salesman_job">Salesmans Job Applications</a></li>

                </ul>
              </li>
            </ul>
          </div>
        </div>
        

        <!-- /sidebar menu -->


      </div>
    </div>

    <!-- top navigation -->
    <div class="top_nav">
      <div class="nav_menu">
        <nav>
          <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
          </div>
          <ul class="nav navbar-nav navbar-right">
            <li class="">
              <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
               <img src="/production_files/images/user.png" alt="">Admin
               <span class=" fa fa-angle-down"></span>
             </a>
             <ul class="dropdown-menu dropdown-usermenu pull-right">
              <li ><a id="logout" href="#"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </div>
  <!-- /top navigation -->

  <!-- page content -->

  <div class="right_col"  role="main">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="container" id="retailer_container">
        <div>
          <table class="table table-bordered table-hover" id="retailerjobs_table" class="display" width="100%">
          </table>
          <p> Showing  
            <span id="page">1</span> of 
            <span id="totalPages">20</span> pages 
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->

  <!-- footer content -->
  <footer style="margin-top: -12px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
      All rights reserved . Crafted with <span>&hearts;</span> at <a href="http://centilliontech.in" target="new">Centillion</a>
    </div>
    <div class="clearfix"></div>
  </footer>
  <!-- /footer content -->
</div>
</div>



<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- Custom Theme Scripts -->
<script src="/build/js/custom.min.js"></script>
<script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="/panel_js/retailer_job.js"></script>

</body>
</html>