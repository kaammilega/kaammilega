<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use App\Job;
use App\Experience;
use App\Posting;
use App\Image;
use App\Applicant;



class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    //relationships

    public function experiences(){
        return $this->hasMany('App\Experience');
    }


    public function applicants()
    {
        return $this->hasMany('App\Applicant');
    }


    public function postings()
    {
        return $this->hasMany('App\Posting');
    }

     public function image()
    {
        return $this->hasOne('App\Image');
    }


        public function retailerjobs()
        {
               $retailers= User::select('id','name','gender','mobile','salary_basis')->where('type','retailer')->get();

                foreach($retailers as $retailer)
                {
                    $job_array=array();
                    $jobs=Posting::where('user_id',$retailer->id)->where('job_id','!=','0')
                    ->pluck('job_id');

                    foreach($jobs as $job)
                    {

                        array_push($job_array,Job::select('title','salary_basis')->where('id',$job)->first());
                    }
                    $retailer['jobs']=$job_array;               

                }
                return $retailers;
            
                

        }
}
