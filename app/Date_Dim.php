<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Date_Dim extends Model {

    protected $guarded = [];

    protected $dates = [];
    protected $table = 'date_dim';

    public static $rules = [
        // Validation rules
    ];

    // Relationships

}
