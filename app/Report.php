<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model {

    protected $table = 'reports';

    protected $guarded = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
}
