<?php 

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;


use App\Image;

class ImagesController extends Controller {

	public function shopImages($sId)
	{
		$shopImages = Image::where('shop_id',$sId)->pluck('url');
		return $shopImages;
	}

	public function deleteShopImage(Request $request)
	{

		$shopimage = Image::where('shop_id',$request['sId'])
		->where('url',$request['imgURI'])
		->delete();

		$imguri = $request['imgURI'];

		if(unlink($imguri)){
			return "Image Deleted Successfully";
		}
		else{
			return "Image not Deleted";
		}

	}
	public function deleteSalesmanImage(Request $request)
	{

		$salesmanimage = Image::where('user_id',$request['sId'])
		->where('url',$request['imgURI'])
		->delete();

		$imguri = $request['imgURI'];

		if(unlink($imguri)){
			return "Image Deleted Successfully";
		}
		else{
			return "Image not Deleted";
		}

	}



	public function saveSalesmanImage(Request $request)
	{
		$salesmanImg = Image::where('user_id',$request['sId'])->first();
		$salesmanImg->url = $request['imageURI'];
		$salesmanImg->save();

		if(!$salesmanImg)
		{
			echo "Failed to upload profile picture!!!";
		}
		else
		{
			echo "Profile picture uploaded successfully!!";
		}
	}

	public function addShopImage(Request $request){

		$image = new Image;
		$image->shop_id = $request['sId'];
		$image->url = $request['url'];
		$image->save();

		return "Image Saved successfully";


	}


	public function uploadSalesmanImage(Request $request)
    {

     $uploadpath = 'assets/images/users/salesman';
     $file = Input::file('file');
     // echo $file; 
     $filename = rand().'.png';
     //$filePath = '/assets/images/users/salesman/'.$filename;
     $file->move($uploadpath,$filename);
     chmod('assets/images/users/salesman/'.$filename, 0777);
     echo $filename;

  }

  public function uploadShopImage(Request $request)
    {

     $uploadpath = 'assets/images/shops';
     $file = Input::file('file');
     // echo $file; 
     $filename = rand().'.png';
     $filePath = '/assets/images/shops/'.$filename;
     $file->move($uploadpath,$filename);
     chmod('assets/images/shops/'.$filename, 0777);
     echo $filename;

  }

}
