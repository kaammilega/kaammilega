<?php namespace 
App\Http\Controllers;
use DB;
use Carbon\Carbon;
use App\Applicant;
use App\Date_Dim;
use App\Job;
use App\User;
use App\Posting;
use App\Image;
use App\Shop;
use App\Department;
use App\Jobtitle;;
use Illuminate\Http\Request;

class Admin_PanelsController extends Controller 
{


	public function all()
	{

		$data = [];

   	//retailer


		//lower
		
		$date = date("Y/m/d");
		$year = date("Y");
		$month = date('m');

		$week_no=Date_Dim::select('week_starting_monday')->where('d_date','=',$date)->get();

		//week 1
		$last_week_1=$week_no[0]->week_starting_monday-1;
		

		$m_date_1=Date_Dim::select('d_date')->where([
			['day_name','=','Monday'],
			['week_starting_monday','=',$last_week_1],
			['year','=',$year],
			])->get();

		$s_date_1=Date_Dim::select('d_date')->where([
			['day_name','=','Sunday'],
			['week_starting_monday','=',$last_week_1],
			['year','=',$year],
			])->get();

		$start_time_1=$m_date_1[0]->d_date." 00:00:00";
		$end_time_1=$s_date_1[0]->d_date." 23:59:59";

		

		//week 2
		$last_week_2=$week_no[0]->week_starting_monday-2;

		$m_date_2=Date_Dim::select('d_date')->where([
			['day_name','=','Monday'],
			['week_starting_monday','=',$last_week_2],
			['year','=',$year],
			])->get();

		$s_date_2=Date_Dim::select('d_date')->where([
			['day_name','=','Sunday'],
			['week_starting_monday','=',$last_week_2],
			['year','=',$year],
			])->get();

		$start_time_2=$m_date_2[0]->d_date." 00:00:00";
		$end_time_2=$s_date_2[0]->d_date." 23:59:59";

	//weeks
		$data['retailer']=count(User::select('id')->where('type','=','retailer')->get());

		$retail_user_week_1 =count(User::select('id')->whereBetween('created_at',[$start_time_1,$end_time_1])->where('type','retailer')->get());

		$retail_user_week_2 =count(User::select('id')->whereBetween('created_at',[$start_time_2,$end_time_2])->where('type','retailer')->get());

		if( $retail_user_week_2 == 0)
		{
			$retailer_per = $retail_user_week_1;
		}
		else
		{
			$retailer_per = $retail_user_week_1-$retail_user_week_2/$retail_user_week_2;
		}

		
		
		
		

	//salesman
		//weeks
		$data['salesman']=count(User::select('id')->where('type','=','salesman')->get());

		$sale_user_week_1 =count(User::select('id')->whereBetween('created_at',[$start_time_1,$end_time_1])->where('type','salesman')->get());

		$sale_user_week_2 =count(User::select('id')->whereBetween('created_at',[$start_time_2,$end_time_2])->where('type','salesman')->get());

		if( $sale_user_week_2 == 0)
		{
			$salesman_per = $sale_user_week_1;
		}
		else
		{
			$salesman_per = $sale_user_week_1-$sale_user_week_2/$sale_user_week_2;
		}


		

   	//job posted

		
		//weeks
		

		$data['jobs']=count(Job::select('id')->get());

		$job_user_week_1 =count(Job::select('id')->whereBetween('created_at',[$start_time_1,$end_time_1])->get());


		$job_user_week_2 =count(Job::select('id')->whereBetween('created_at',[$start_time_2,$end_time_2])->get());

		if( $job_user_week_2 == 0)
		{
			$job_per = $job_user_week_1;
		}
		else
		{

			$job_per=$job_user_week_1-$job_user_week_2/$job_user_week_2;
		}


   	//placement


		//weeks
		$data['placement']=count(Applicant::select('user_id')->where('status','selected')->get());

		$place_user_week_1 =count(Applicant::select('user_id')->whereBetween('created_at',[$start_time_1,$end_time_1])->where('status','=','selected')->get());

		$place_user_week_2 =count(Applicant::select('user_id')->whereBetween('created_at',[$start_time_2,$end_time_2])->where('status','=','selected')->get());

		if( $place_user_week_2 == 0)
		{
			$placement_per = $place_user_week_1;
		}
		else
		{
			$placement_per = $place_user_week_1 - $place_user_week_2 / $place_user_week_2;
		}

		// weeks
		$data['applicant']=count(Applicant::select('user_id')->distinct()->where('status','=','applied')->get());

		$app_user_week_1 = count(Applicant::select('user_id')->distinct()->whereBetween('created_at',[$start_time_1,$end_time_1])->where('status','=','applied')->get());



		$app_user_week_2 = count(Applicant::select('user_id')->distinct()->whereBetween('created_at',[$start_time_2,$end_time_2])->where('status','=','applied')->get());


		if( $app_user_week_2 == 0)
		{
			$applicant_per = $app_user_week_1;
		}
		else
		{
			$applicant_per = $app_user_week_1 - $app_user_week_2 / $app_user_week_2;
		}

		//weeks
		$data['vacancies']=Job::select(DB::raw('sum(requirement) as open_position'))->first();
		$vacan_user_week_1_get=Job::select(DB::raw('sum(requirement) as open_position'))->whereBetween('created_at',[$start_time_1,$end_time_1])->get();

		$vacan_user_week_2_get=Job::select(DB::raw('sum(requirement) as open_position'))->whereBetween('created_at',[$start_time_2,$end_time_2])->get();

		$vacan_user_week_1 = $vacan_user_week_1_get[0]->open_position;
		$vacan_user_week_2 = $vacan_user_week_2_get[0]->open_position;

		if( $vacan_user_week_2 == 0)
		{
			$vacancies_per = $vacan_user_week_1;
		}
		else
		{
			$vacancies_per = $vacan_user_week_1 - $vacan_user_week_2 / $vacan_user_week_2;
		}

		
		$data['retailer_per']=$retailer_per;
		$data['salesman_per']=$salesman_per;
		$data['jobs_per']=$job_per;
		$data['placement_per']=$placement_per;
		$data['applicant_per']=$applicant_per;
		$data['vacancies_per']=$vacancies_per;
		




		$now = Carbon::now();
		$months_back=$now->subMonth(12);
		$month_first=[];
		$month_last=[];
		$month_name=[];

		$no_of_jobs=[];
		$no_of_applicants=[];
		$no_of_placements=[];

		$job_title=[];
		// $job_type=[];

		
		for($i=0;$i<12;$i++)
		{
			$year=$months_back->format("Y");
			$month=$months_back->format("F");
			$month_name[$i]=$month;
			$month_f = new Carbon('first day of '.$month.' '.$year);
			$month_l = new Carbon('last day of '.$month.' '.$year);
			$month_first[$i]=$month_f;
			$month_last[$i]=$month_l->addHours(23)->addMinutes(59)->addSeconds(59);
			$months_back->addMonth();


			//graph

			$no_of_jobs[$i]=count(Posting::select('job_id')->where('job_id','>',0)->whereBetween('created_at',[$month_first[$i],$month_last[$i]])->get());

			$no_of_applicants[$i]=count(User::select('name')->whereBetween('created_at',[$month_first[$i],$month_last[$i]])->where('type','=','salesman')->get());

			$no_of_placements[$i]=count(Applicant::select('user_id')->distinct()->whereBetween('created_at',[$month_first[$i],$month_last[$i]])->where('status','=','selected')->get());

		}


		

		$job_ti=Job::select('title','requirement')->orderBy('requirement','desc')->get();

		foreach ($job_ti as $jobti) {
			$jobti['title'] = Jobtitle::where('id',$jobti->title)->pluck('job_title')->first();	
		}


	

		// return $job_ti;


		// $job_ty=Job::select('job_type',DB::raw('count(*) as count_type'))->groupBy('job_type')->orderBy(DB::raw('count(*)'),'desc')->get();


		$current_year_first_month = new Carbon('first day of January'.' '.$year);
		

		$salary_basis=Job::select('salary_basis',DB::raw('count(*) as count_type'))->whereBetween('created_at',[$current_year_first_month,$now])->groupBy('salary_basis')->orderBy(DB::raw('count(*)'),'desc')->get();

		// $job_type_basis=Job::select('job_type',DB::raw('count(*) as count_type'))->whereBetween('created_at',[$current_year_first_month,$now])->groupBy('job_type')->orderBy(DB::raw('count(*)'),'desc')->get();


		$job_title_basis=Job::select('title',DB::raw('count(*) as count_type'))->whereBetween('created_at',[$current_year_first_month,$now])->groupBy('title')->orderBy(DB::raw('count(*)'),'desc')->get();

		foreach ($job_title_basis as $jobtitle) {
			$jobtitle['title'] = Jobtitle::where('id',$jobtitle['title'])->pluck('job_title')->first();
		}

		// return $job_title_basis;

//------------------------------------------------------------------------------------------
		$departments=Department::get();
		$department_count=Department::select('department')->get()->count();
		foreach ($departments as $department) {


			$ids=Shop::where('department',$department->id)->pluck('id');
			if(count($ids) == 0)
			{
				$department['shops_ids']=0;
				$department['count_ids']=0;
				$department['postings']=0;
				$department['postings_count']=0;
				$department['applicant']=0;
				$department['applicant_count']=0;
			}
			else
			{
				$department['shops_ids']=$ids;
				$department['count_ids']=count($ids);

				$department['postings']=Posting::whereIn('shop_id',$department['shops_ids'])->where('job_id','!=',0)->pluck('job_id');
				$department['postings_count']=count($department['postings']);

				$department['applicant']=Posting::whereIn('shop_id',$department['shops_ids'])->where('job_id','!=',0)->pluck('user_id');
				$department['applicant_count']=count($department['applicant']);
			}
			
			
		}
		$sm=array();
		$bm=array();
		$length=count($departments);
		for($i=0;$i<$length;$i++)
		{
			$sm['name']=$departments[$i]['department'];
			// $sm[1]=$departments[$i]['count_ids'];
			// $sm[2]=$departments[$i]['postings_count'];
			$sm['count']=$departments[$i]['applicant_count'];
			array_push($bm,$sm);
		}

		




		
		// upper post

	// top 5
		$data['job_title']=$job_ti;
		$data['department']=$bm;

		// pie 
		$data['salary_basis']=$salary_basis;
		$data['job_title_basis']=$job_title_basis;
		// $data['job_type_basis']=$job_type_basis;

		

		//graph
		$data['no_of_jobs']=$no_of_jobs;
		$data['no_of_applicants']=$no_of_applicants;
		$data['no_of_placements']=$no_of_placements;
		$data['months']=$month_name;
		
		return $data ;




	}

	public function department_check()
	{
		$departments=Department::select('department')->get();
		$department_count=Department::select('department')->get()->count();
		foreach ($departments as $department) {
			
			$ids=Shop::where('department',$department->department)->pluck('id');
			if(count($ids) == 0)
			{
				$department['shops_ids']=0;
				$department['count_ids']=0;
				$department['postings']=0;
				$department['postings_count']=0;
				$department['applicant']=0;
				$department['applicant_count']=0;
			}
			else
			{
				$department['shops_ids']=$ids;
				$department['count_ids']=count($ids);

				$department['postings']=Posting::whereIn('shop_id',$department['shops_ids'])->where('job_id','!=',0)->pluck('job_id');
				$department['postings_count']=count($department['postings']);

				$department['applicant']=Posting::whereIn('shop_id',$department['shops_ids'])->where('job_id','!=',0)->pluck('user_id');
				$department['applicant_count']=count($department['applicant']);
			}
			
			
		}
		$sm=array();
		$bm=array();
		$length=count($departments);
		for($i=0;$i<$length;$i++)
		{
			$sm['nam']=$departments[$i]['department'];
			// $sm[1]=$departments[$i]['count_ids'];
			// $sm[2]=$departments[$i]['postings_count'];
			$sm['count']=(string)$departments[$i]['applicant_count'];
			array_push($bm,$sm);
		}

		
		
		return $bm;
		
		
	}


	public function admin_check(Request $request)
	{

		$user=$request['user'];
		$pass=$request['password'];


		if(empty($user) && !empty($pass))
		{

			//$data["error"]="user not entered";

			return 0;

		}
		elseif(empty($pass) && !empty($user))
		{

			//$data["error"]="password not entered";
			return 1;

		}
		elseif(empty($user) && empty($pass))
		{
			$data["error"]="user and password not entered";

			return 2;
		}
		else
		{
			$result = User::select('type')->where([
				['name','=',$user],
				['password','=',$pass],
				])->first();
			

			if(empty($result))
			{
				return 3;
			}
			else
			{
				return "admin";
			}

		}
	}


	


	public function allhired()
	{
		$retailers=User::where('type','retailer')->get();
		foreach ($retailers as $retailer) 
		{	
			$selected_person=array();
			$job_profile=array();

			$jobs=Posting::where('user_id',$retailer->id)->where('job_id','!=','0')
			->pluck('job_id');

			foreach ($jobs as $job) 
			{
				$user_ids=Applicant::select('job_id','user_id')->where('job_id',$job)->where('status','selected')->get();

				foreach ($user_ids as $user_id) 
				{


					array_push($job_profile,Job::where('id',$user_id->job_id)->first());
					array_push($selected_person,User::where('id',$user_id->user_id)->first());
				}

				$retailer['job']=$job_profile;
				$retailer['person']=$selected_person;

			}
		}
		return $retailers;	

	}



	public function premium_jobs()
	{

		$jobs=array();
		
		$jobs = Job::orderBy('created_at','desc')->limit(5)->get();

		
		
		for ($i=0; $i <count($jobs) ; $i++) 
		{ 

			$jobs_shopid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('shop_id');
			$jobs_retailerid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('user_id');
			$jobs[$i]['retailer']=User::where('id',$jobs_retailerid)->select('id','type',
				'name','email','gender','dob','describe_yourself','salary_basis','mobile','address')->first();
			$jobs[$i]['shop']=Shop::where('id',$jobs_shopid)->select('id','name','contact','address','working_hours','established_year','department')->first();
			$jobs[$i]['shop']['department'] = Department::where('id',$jobs[$i]['shop']['department'])->pluck('department')->first();

			$jobs[$i]['title'] = Jobtitle::where('id',$jobs[$i]->title)->pluck('job_title')->first();

			$image_count=count(Image::where('shop_id',$jobs_shopid)->pluck('url'));
			
			if($image_count>0)
			{
				$jobs[$i]['shop']['images']=Image::where('shop_id',$jobs_shopid)->pluck('url');
			}

			
		}

		return $jobs;
	}

	public function d5_salesman()
	{
		// $now = Carbon::now()->setTimezone('Asia/Kolkata');
		// $current=$now->toDateTimeString();
		// $past5=$now->subDays(5)->hour(00)->minute(00)->second(00)->toDateTimeString();
		
		
		$salesman = User::where('type','salesman')->whereBetween('created_at',[$current,$past5])->get();

		$image = new Image;

		foreach ($salesman as $saleperson) {
			$saleperson['image'] = $image->where('user_id',$saleperson->id)->pluck('url')->first();
		}

		return $salesman;


	}

	public function premium_salesman()
	{
		$now = Carbon::now()->setTimezone('Asia/Kolkata');
		$current=$now->toDateTimeString();
		$past5=$now->subDays(5)->hour(00)->minute(00)->second(00)->toDateTimeString();
		
		
		$salesman = User::where('type','salesman')->whereBetween('created_at',[$current,$past5])->get();

		$image = new Image;

		foreach ($salesman as $saleperson) {
			$saleperson['image'] = $image->where('user_id',$saleperson->id)->pluck('url')->first();
		}

		return $salesman;


	}

	public function allSalesman(Request $request)
	{

		$salesmans = User::where('type','salesman')->get();
		$data=array();
		
		foreach ($salesmans as $s) {
			$name = $s->name;	
			$email = $s->email;	
			$gender = $s->gender;	
			$mobile = $s->mobile;	
			$address = $s->address;	
			$pincode = $s->pincode;

			$user = array();
			$user = [$name,$email,$mobile,$address,$pincode];

			array_push($data, $user);


		}
		

		$pageNumber = $request['pageNumber'];
		$recordstoShow = $request['recordsToShow'];
		$show = (int)$pageNumber - 1;
		$show *= (int)$recordstoShow; 
		$final = array();
		$length = $show + $recordstoShow + 1;
		for ($i=$show; $i < $length ; $i++) { 
			if(isset($data[$i])){
				array_push($final,$data[$i]);
			}
		}
		$result['data'] = $final;
		$result['recordsTotal'] = count($data);
		$result['recordsFiltered'] = count($data);
		return $result;

	}


	public function allRetailer(Request $request)
	{
		$retailers = User::where('type','retailer')->get();
		$data=array();
		
		foreach ($retailers as $r) {
			$name = $r->name;	
			$email = $r->email;	
			$gender = $r->gender;	
			$mobile = $r->mobile;	
			$address = $r->address;	
			$pincode = $r->pincode;

			$user = array();
			$user = [$name,$email,$mobile,$address,$pincode];

			array_push($data, $user);


		}
		

		$pageNumber = $request['pageNumber'];
		$recordstoShow = $request['recordsToShow'];
		$show = (int)$pageNumber - 1;
		$show *= (int)$recordstoShow; 
		$final = array();
		$length = $show + $recordstoShow + 1;
		for ($i=$show; $i < $length ; $i++) { 
			if(isset($data[$i])){
				array_push($final,$data[$i]);
			}
		}
		$result['data'] = $final;
		$result['recordsTotal'] = count($data);
		$result['recordsFiltered'] = count($data);
		return $result;


	}

	public function allretailerjobs(Request $request)
	{
		/*$user = new User;
		$ab=$user->retailerjobs();
		return $ab;*/

		$retailers = User::where('type','retailer')->get();

		
		$data = array();
		foreach ($retailers as $r) {
			$name = $r->name;
			$gender = $r->gender;
			$mobile = $r->mobile;


			$job_ids = Posting::where('user_id',$r->id)->where('job_id','!=',0)->pluck('job_id');
			$user = array();
			foreach ($job_ids as $jobid) {
				$job = Job::where('id',$jobid)->first();	
				$title = Jobtitle::where('id',$job->title)->pluck('job_title')->first();
				$user = [$name,$gender,$mobile,$title];
				array_push($data, $user);
			}


		}

		$pageNumber = $request['pageNumber'];
		$recordstoShow = $request['recordsToShow'];
		$show = (int)$pageNumber - 1;
		$show *= (int)$recordstoShow; 
		$final = array();
		$length = $show + $recordstoShow + 1;
		for ($i=$show; $i < $length ; $i++) { 
			if(isset($data[$i])){
				array_push($final,$data[$i]);
			}
		}
		$result['data'] = $final;
		$result['recordsTotal'] = count($data);
		$result['recordsFiltered'] = count($data);
		return $result;
	}

	public function allsalesmanjobs(Request $request)
	{
		
		$applicants = Applicant::all();

		$data = array();
		foreach ($applicants as $applicant) {
			$name = User::where('id',$applicant->user_id)->where('type','salesman')->pluck('name')->first();
			$gender= User::where('id',$applicant->user_id)->where('type','salesman')->pluck('gender')->first();
			$mobile = User::where('id',$applicant->user_id)->where('type','salesman')->pluck('mobile')->first();
			$salary = User::where('id',$applicant->user_id)->where('type','salesman')->pluck('salary_basis')->first();
			$jobtitleId = Job::where('id',$applicant->job_id)->pluck('title')->first();
			$job_title = Jobtitle::where('id',$jobtitleId)->pluck('job_title')->first();
			$user = array();
			$user = [$name,$gender,$mobile,$job_title,$salary];
			array_push($data, $user);
		}		

		$pageNumber = $request['pageNumber'];
		$recordstoShow = $request['recordsToShow'];

		$show = (int)$pageNumber - 1;
		$show *= (int)$recordstoShow; 

		$final = array();

		$length = $show + $recordstoShow + 1;



		for ($i=$show; $i < $length ; $i++) { 
			if(isset($data[$i])){
				array_push($final,$data[$i]);
			}
		}


		$result['data'] = $final;
		$result['recordsTotal'] = count($data);
		$result['recordsFiltered'] = count($data);

		return $result;

	}

}
