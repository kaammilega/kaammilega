<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Applicant;
use App\Posting;
use App\User;
use App\Job;
use App\Image;
use App\Shop;
use App\Jobtitle;
use App\Department;
use App\Experience;

class ApplicantController extends Controller
{
    public function add(Request $request)
    {
        $applicant = new Applicant;
        $applicant->user_id = $request['uId'];
        $applicant->job_id = $request['jId'];
        $applicant->status = 'applied';
        $applicant->save();
        $retailer_id = Posting::where('job_id',$request['jId'])->pluck('user_id')->first();
        $job_id = $request['jId'];
        $a_id = $request['uId'];

        $this->notifyRetailer($retailer_id,$job_id,$a_id);
        return "Application Submitted Successfully";
    }

    function notifyRetailer($user_id,$job_id,$a_id){
        $content = array(
            "en" => "You have new Applicant"
            );

        $device = array();
        array_push($device,User::where('id',$user_id)->pluck('device')->first());
        // dd($device);
        $fields = array(
            'app_id' => "31e759f6-a64f-44ea-81e1-ee32e49d019c",
            'include_player_ids' => $device,
            'data' => array("job_id"=>$job_id,"a_id"=>$a_id,"type"=>"retailer"),
            'contents' => $content
            );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ZjgyNzU1NDYtMWRhOC00ZWMyLWI4NTItMjUyOGQyZjk4MjYz'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        // return $response;

    }

    public function getByRetailer($rId)
    {
        $jobs=Posting::where('user_id',$rId)->where('job_id','!=','0')->get();
        // return $job_ids;
        $users=array();

        foreach($jobs as $job)
        {
            $job['applicants']=Applicant::where('job_id',$job->job_id)->get();
            
            foreach($job['applicants'] as $applicant)
            {
                $applicant['user']=User::where('id',$applicant->user_id)->get();

                foreach($applicant['user'] as $user)
                {
                    if (in_array($user,$users))
                    {
                        //DO NOTHING
                    }
                    else
                    {
                        array_push($users,$user);
                    }
                }
            }
        }

        foreach($users as $user)
        {
            $id=$user->id;

            $user['status']=Applicant::where('user_id',$id)->pluck('status')->first();
            $user['image']=Image::where('user_id',$id)->pluck('url')->first();
            
            $jobs=array();
            $shops=array();

            $job_ids=Applicant::where('user_id',$id)->pluck('job_id');
            foreach($job_ids as $job_id)
            {
                $job=Job::where('id',$job_id)->first();
                $job['title'] = Jobtitle::where('id',$job->title)->pluck('job_title')->first();                                
                array_push($jobs,$job);

                $shop_id=Posting::where('job_id',$job_id)->pluck('shop_id')->first();
                $shop=Shop::where('id',$shop_id)->first();
                $shop['department'] = Department::where('id',$shop->department)->pluck('department')->first();
                if(in_array($shop,$shops))
                {
                    //DO NOTHING
                }
                else
                {
                    array_push($shops,$shop);
                }

            }

            $user['jobs']=$jobs;
            $user['shops']=$shops;


        }
        if(count($users)>0)
        {
            return $users;
        }
        else
        {
            return "noApplicants";
        }

    }

    public function changeStatus(Request $request)
    {
        $user_id=$request['uId'];
        $job_id=$request['jId'];
        $status=$request['status'];
        Applicant::where('user_id',$user_id)->where('job_id',$job_id)->update(['status'=>$status]);
        $selectcount = Applicant::where('status','selected')->where('job_id',$request['jId'])->select('status')->count();
        Job::where('id',$request['jId'])->update(['filled'=>$selectcount]);
        $req = Job::where('id',$request['jId'])->pluck('requirement')->first();
        if($req == $selectcount){
            Posting::where('job_id',$request['jId'])->update(['status'=>'closed']);
            Applicant::where('job_id',$request['jId'])->where('status','!=','selected')->update(['status'=>'rejected']);
        }
        else{
            Posting::where('job_id',$request['jId'])->update(['status'=>'open']);
            Applicant::where('job_id',$request['jId'])->where('status','!=','selected')->where('status','!=',$status)->update(['status'=>'applied']);
        }
        $this->notifyApplicant($status,$user_id,$job_id);
        return "Status Updated";
    }


    function notifyApplicant($status,$user_id,$job_id){
        $content = array(
            "en" => "Your Job status has changed"
            );

        $device = array();
        array_push($device,User::where('id',$user_id)->pluck('device')->first());
        // dd($device);
        $fields = array(
            'app_id' => "31e759f6-a64f-44ea-81e1-ee32e49d019c",
            'include_player_ids' => $device,
            'data' => array("status"=>$status,"job_id"=>$job_id,"type"=>"applicant"),
            'contents' => $content
            );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ZjgyNzU1NDYtMWRhOC00ZWMyLWI4NTItMjUyOGQyZjk4MjYz'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        // return $response;

    }

    public function checkStatus(Request $request)
    {
        $user_id=$request['uId'];
        $job_id=$request['jId'];

        $status = Applicant::where('user_id',$user_id)->where('job_id',$job_id)->pluck('status')->first();

        if ($status)
        {
            return $status;
        }
        else
        {
            return "notApplied";
        }
    }



        /*public function getApplicant($aId)
        {

            $applicant=User::where('id',$aId)->first();
            $applicant['image']=Image::where('user_id',$aId)->pluck('url')->first();

            $job_ids=Applicant::where('user_id',$aId)->pluck('job_id');
            $jobs=array();

            foreach($job_ids as $job_id)
            {
                $job=Job::where('id',$job_id)->first();
                $shop_id=Posting::where('job_id',$job_id)->pluck('shop_id')->first();
                $shop=Shop::where('id',$shop_id)->first();
                $job['shop']=$shop;
                $job['applicant_status'] = Applicant::where('user_id',$aId)->where('job_id',$job_id)->pluck('status')->first();
                array_push($jobs,$job);
            }

            $total_exp =Experience::where('user_id',$aId)->get();
            $array_exp=[];
            $sum_exp=0;
            foreach ($total_exp as $each_exp) {

               $sum_exp +=$each_exp->years;
               array_push($array_exp,$each_exp);
           }
           $applicant['total_exp']=$sum_exp;
           $applicant['experince']=$array_exp;
           $applicant['jobs']=$jobs;

           return $applicant;
       }*/


       public function getApplicantDetail(Request $request)
       {

        $aId = $request['aId'];
        $rId = $request['rId'];
        $applicant=User::where('id',$aId)->first();
        $applicant['image']=Image::where('user_id',$aId)->pluck('url')->first();

        $jobids = Posting::where('user_id',$rId)->where('job_id','!=',0)->pluck('job_id');
        $job_ids=Applicant::where('user_id',$aId)->pluck('job_id');

        $jobidArray =  array();
        foreach ($jobids as $j) {
            foreach ($job_ids as $ji) {
                if($j === $ji){
                    array_push($jobidArray, $ji);
                }
            }
        }


        $jobs=array();

        foreach($jobidArray as $job_id)
        {
            $job=Job::where('id',$job_id)->first();
            $job['title'] = Jobtitle::where('id',$job->title)->pluck('job_title')->first();
            $shop_id=Posting::where('job_id',$job_id)->pluck('shop_id')->first();
            $shop=Shop::where('id',$shop_id)->first();
            $shop['department'] = Department::where('id',$shop->department)->pluck('department')->first();
            $job['shop']=$shop;
            $job['applicant_status'] = Applicant::where('user_id',$aId)->where('job_id',$job_id)->pluck('status')->first();
            array_push($jobs,$job);
        }

        $total_exp =Experience::where('user_id',$aId)->get();
        $array_exp=[];
        $sum_exp=0;
        foreach ($total_exp as $each_exp) {

         $sum_exp +=$each_exp->years;
         $each_exp['department'] = Department::where('id',$each_exp->department)->pluck('department')->first();
         array_push($array_exp,$each_exp);
     }
     $applicant['total_exp']=$sum_exp;
     $applicant['experince']=$array_exp;
     $applicant['jobs']=$jobs;

     return $applicant;
 }


//chinar---------------------------------------------------------------
 public function allJobId($jId)
 {

    $users_ids= Applicant::where('job_id',$jId)->select('user_id')->get();
    $users= User::whereIn('id',$users_ids)->get();

        // $users = User::whereIn('id',Applicant::where('job_id',$jId)->select('user_id')->get())->get();

    $users=User::whereIn('id',$users_ids)->get();

    foreach ($users as $user) {
        $user['image']=Image::where('user_id',$user->id)->pluck('url')->first();
        $user['status']=Applicant::where('user_id',$user->id)->pluck('status')->first();

    }
    if(count($users)>0)
    {
        return $users;
    }
    else
    {
        return "noApplicants";
    }

}
        //-----------------------------------------------------//
public function allShopId ($sId)
{

    $jobs_filter=array();
    $jobs_id=Posting::where('shop_id',$sId)->where('job_id','!=',0)->pluck('job_id');
    $users_id=Applicant::whereIn('job_id',$jobs_id)->select('user_id')->get();
    $users=User::whereIn('id',$users_id)->get();
    foreach ($users as $user) 
    {
        $user['image']=Image::where('user_id',$user->id)->pluck('url')->first();
        $user['status']=Applicant::where('user_id',$user->id)->pluck('status')->first();
        $userjobs_all=Applicant::where('user_id',$user->id)->pluck('job_id');
           // dd(json_encode( $userjobs_all));
        if(isset($userjobs_all))
        {
            for ($i=0; $i < count($jobs_id); $i++) 
            { 

                for ($j=0; $j <count($userjobs_all) ; $j++) 
                { 
                 if($jobs_id[$i] == $userjobs_all[$j] )
                 {
                    $jobs=$userjobs_all[$j];
                                       // dd(json_encode($jobs));
                    array_push($jobs_filter,$jobs);

                }  
            }


        }

    }
    $jobs = Job::whereIn('id',$jobs_filter)->get();

    foreach ($jobs as $job) {
        $job['title'] = Jobtitle::where('id',$job->title)->pluck('job_title')->first();
    }

    $user['jobs'] = $jobs;
    unset($jobs_filter[0]);




}


return $users;
}

   //------------------------------------------------------chinar-------------------------

public function getAppliedJobs($sId)
{

    $job_ids = Applicant::where('user_id',$sId)->pluck('job_id');

            if(count($job_ids)>0)  // return $job_ids;
            {
                foreach ($job_ids as $job_id) 
                {
                    $jobs[] = Job::find($job_id);
                }

                for ($i=0; $i <count($jobs) ; $i++) 
                { 

                    $jobs_shopid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('shop_id');
                    $jobs_retailerid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('user_id');
                    $jobs[$i]['retailer']=User::where('id',$jobs_retailerid)->select('id','type',
                        'name','email','gender','dob','describe_yourself','salary_basis','mobile','address')->first();
                    $jobs[$i]['shop']=Shop::where('id',$jobs_shopid)->select('id','name','department','contact','address','working_hours','established_year')->first();
                    $jobs[$i]['shop']['department'] = Department::where('id',$jobs[$i]['shop']->department)->pluck('department')->first();
                    $jobs[$i]['title'] = Jobtitle::where('id',$jobs[$i]->title)->pluck('job_title')->first();


                    $image_count=count(Image::where('shop_id',$jobs_shopid)->pluck('url'));

                    if($image_count>0)
                    {
                        $jobs[$i]['shop']['images']=Image::where('shop_id',$jobs_shopid)->pluck('url');
                    }


                }

                return $jobs;
            }
            else
            {

                return "no_jobs";
            }
        }


    }

