<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Retailer;
use App\Shop;
use App\Salesman;
use App\Image;
use App\Experience;
use App\Convert;
use App\Job;

class LoginsController extends Controller {


    public function authenticate(Request $request)
    {

        $matchThese = ['email' => $request['email']];
        $results = User::where($matchThese)->get();
        if(count($results)>0)
        {
            $input = $results[0]->password;
            
            if(Hash::check($request['password'],$input)){
                $user = User::where('email',$request['email'])->update(['device'=>$request['device']]);
                return 'loginSuccess';
            }
            else
            {
                return "IncorrectPassword";
            }
        }
        else
        {
            return "IncorrectEmail";
        }

    }

    public function verifymail(Request $request)
    {
     $matchThese = ['email' => $request['email']];
     $results = User::where($matchThese)->get();
     if(count($results)>0)
     {
        return $results[0]->type.":".$results[0]->id.":".$results[0]->name;
    }
    else
    {
        return "IncorrectEmail";
    }

}



}
