<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Experience;
use App\User;
use App\Image;
use App\Department;

class UsersController extends Controller {


	public function allSalesman(){

		$salesman = User::where('type','salesman')->get();

		$image = new Image;

		foreach ($salesman as $saleperson) {
			$saleperson['image'] = $image->where('user_id',$saleperson->id)->pluck('url')->first();
		}

		return $salesman;


	}

	public function allRetailer()
	{
		
		return User::where('type','retailer')->get();

	}

	public function get($id)
	{
		$user = User::find($id);

		if($user->type == "retailer")
		{
			return $user;
		}
		else
		{
			$total_exp = Experience::where('user_id',$user->id)->pluck('years');
			if(count($total_exp)>0)
			{
				$sum_exp=0;
				foreach ($total_exp as $exp) {

					$sum_exp += $exp;
				}
				$image = new Image;
				$user['image'] = $image->where('user_id',$id)->pluck('url')->first();
				$user['experience']= Experience::where('user_id',$user->id)->orderBy('to_year','desc')->first();
				$user['experience']['department'] = Department::where('id',$user['experience']['department'])->pluck('department')->first();

				$user['total']=$sum_exp;
				return $user;
			}
			else
			{
				$image = new Image;
				$user['image'] = $image->where('user_id',$id)->pluck('url')->first();
				$user['experience']="0";
				$user['total']="fresher";
				return $user;
			}

		}
		

		

	}

	public function add(Request $request){


		$user = new User;

		if($request['type'] == 'retailer')
		{

			$user->type = 'retailer';
			$user->name = $request['name'];
			$user->email = $request['email'];
			$user->password = Hash::make($request['password']);
			$user->mobile= $request['mobile'];
			$user->address = $request['address'];
			$user->pincode = $request['pincode'];
			/*$user->city = $request['city'];
			$user->state = $request['state'];
			$user->pincode = $request['pincode'];
			$user->country= $request['country'];*/
			$user->gender= $request['gender'];
			$user->dob= $request['dob'];
			$user->describe_yourself= 'r';
			$user->salary_basis= 'r';

			$user->save();	

			return $user->id;
		}

		else if ( $request['type'] == 'salesman'){


			$user->type = 'salesman';
			$user->name = $request['name'];
			$user->email = $request['email'];
			$user->password = Hash::make($request['password']);
			$user->mobile= $request['mobile'];
			$user->address = $request['address'];
			$user->pincode = $request['pincode'];
			/*$user->city = $request['city'];
			$user->state = $request['state'];
			$user->pincode = $request['pincode'];
			$user->country= $request['country'];*/
			$user->gender= $request['gender'];
			$user->dob= $request['dob'];
			$user->describe_yourself= $request['describe_yourself'];
			$user->salary_basis= $request['salary_basis'];

			$user->save();	

			$image = new Image;
			$image->url = $request['salesmanImg'];
			$image->user_id = $user->id;

			$image->save();

			return $user->id;

		}

		else {

			return "Type not matched";
		}

	}




	public function put(Request $request, $id){


		User::where('id',$id)->update($request->all());	

		return "User Updated";


	}

	public function remove($id){

		$user = User::find($id);

		$user->delete();

		return "User deleted";

	}

	public function update_user(Request $request)
	{
		
		User::where('id',$request['id'])->update($request->all());
		return "User Updated";
	}

}
