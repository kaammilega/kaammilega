<?php 

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers;
use App\Department;
use App\Shop;
use App\Image;
use App\Posting;
use Carbon\Carbon;
use DateTime;


class ShopsController extends Controller {

	public function all()
	{	

		$shops = Shop::all();

		foreach ($shops as $shop) {
			$shop['image'] = Image::where('shop_id',$shop->id)->select('url')->get();
			$shop['department'] = Department::where('id',$shop->department)->pluck('department')->first();
		}

		return $shops;
	}

	public function get($id)
	{
		
		$shop = Shop::find($id);

		$shop['department'] = Department::where('id',$shop->department)->pluck('department')->first();
		
		$shopImage= Image::where('shop_id',$id)->pluck('url');


		if(count($shopImage)>0)
		{
			$shop['images']= Image::where('shop_id',$id)->pluck('url');


			// $fromTime_data = new Carbon($shop->fromTime);
			// $toTime_data = new Carbon($shop->toTime);

			// $from_time=$fromTime_data->format('H:i:s');
			// $to_time=$toTime_data->format('H:i:s');


			// $datetime1 = new DateTime($from_time);
			// $datetime2 = new DateTime($to_time);

			// $time=$datetime2->diff($datetime1);
			// $time_format=$time->format('%H');
			return $shop;
		}
		else
		{
			return $shop;
		}
	}


	public function add(Request $request)
	{

		$shop = new Shop;
		//$images_count=array();
		$shop->name = $request['name'];
		$shop->contact = $request['contact'];
		$shop->address = $request['address'];
		$shop->pincode = $request['pincode'];
		$shop->department = $request['shopDepartment'];
		$shop->established_year = $request['established_year'];

		$fromTime_data = new Carbon($request['fromTime']);
		$toTime_data = new Carbon($request['toTime']);

		$from_time=$fromTime_data->format('H:i');
		$to_time=$toTime_data->format('H:i');

		$shop->fromTime = $from_time; //value set
		$shop->toTime = $to_time; //value set

		$datetime1 = new DateTime($from_time);
		$datetime2 = new DateTime($to_time);
		
		$time = $datetime2->diff($datetime1);

		$shop->working_hours=$time->format('%H.%i'); //differenc in time 
		$shop->save();

		if(!empty($request['shop_images']))
		{
			$images_array=explode(',',$request['shop_images']);
			for ($i=0; $i < count($images_array); $i++) {
				$image = new Image;
				$image->shop_id=$shop->id;
				$image->url=$images_array[$i];
				$image->save();
			}
		}


		$posting = new Posting;

		$posting->user_id = $request['rId'];	
		$posting->shop_id = $shop->id;	
		
		$posting->save();

		return "Shop added Successfully";
	}

	public function put(Request $request, $id)
	{

		Shop::where('id',$id)->update($request->all());	

		return "Shop Updated";
	}


	public function remove($id)
	{
		$shop = Shop::find($id);

		$shop->delete();

		return "Shop deleted";
	}


//Other methods

	public function retailerShops($rId)
	{

		$shopsIds = Posting::where('user_id',$rId)->pluck('shop_id')->all();

		$Ids = array_unique($shopsIds);

		$rShops = array();

		if($Ids)
		{
			foreach ($Ids as $shopId) {

				$shop = Shop::where('id',$shopId)->first();
				$shop['department'] = Department::where('id',$shop->department)->pluck('department')->first();
				array_push($rShops,$shop); 

			}
			return $rShops;
		}
		else
		{
			return "noShops";
		}
		
	}
	public function update(Request $request)
	{
		$shop = new Shop;
		$fromTime_data = new Carbon($request['fromTime']);
		$toTime_data = new Carbon($request['toTime']);

		$from_time=$fromTime_data->format('H:i');
		$to_time=$toTime_data->format('H:i');

		$shop->fromTime = $from_time; //value set
		$shop->toTime = $to_time; //value set

		$datetime1 = new DateTime($from_time);
		$datetime2 = new DateTime($to_time);
		
		$time = $datetime2->diff($datetime1);

		// $shop->working_hours=$time->format('%H.%i');
	
		Shop::where('id',$request['id'])->update([
            'name'=>$request['name'],
            'contact'=>$request['contact'],
            'address'=>$request['address'],
            'established_year'=>$request['established_year'],
            'pincode'=>$request['pincode'],
            'department' =>$request['shopDepartment'],
            'fromtime'=>$from_time,
            'totime'=>$to_time,
            'working_hours'=>$time->format('%H.%i')
        ]);

        
      
		return "Shop Updated";
	}


	public function shopnames()
	{
		$names =Shop::select('id','name')->orderBy('name')->get();

		return $names;
	}

}
