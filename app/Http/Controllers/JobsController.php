<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Shop;
use App\Job;
use App\Posting;
use App\Department;
use App\Jobtitle;
use App\Applicant;
use App\User;
use App\Image;
use App\Experience;
use DB;

class JobsController extends Controller {

	public function all()
	{
		$images=array();
		$job_ids = Posting::where('status','open')->pluck('job_id');
		$jobs=Job::whereIn('id',$job_ids)->get();
		for ($i=0; $i <count($jobs) ; $i++) 
		{ 

			$jobs_shopid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('shop_id');
			$jobs_retailerid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('user_id');
			$jobs[$i]['retailer']=User::where('id',$jobs_retailerid)->select('id','type',
				'name','email','gender','dob','describe_yourself','salary_basis','mobile','address')->first();
				$jobs[$i]['title'] = Jobtitle::where('id',$jobs[$i]->title)->pluck('job_title')->first();
			$jobs[$i]['shop']=Shop::where('id',$jobs_shopid)->select('id','name','contact','address','working_hours','established_year','department')->first();
			$jobs[$i]['shop']['department'] = Department::where('id',$jobs[$i]['shop']->department)->pluck('department')->first();
			$image_count=count(Image::where('shop_id',$jobs_shopid)->pluck('url'));
			
			if($image_count>0)
			{
				$jobs[$i]['shop']['images']=Image::where('shop_id',$jobs_shopid)->pluck('url');
			}

			
		}
		return $jobs;
	}


	public function get($id)
	{
		
		$jobs=Job::where('id',$id)->get();


		foreach($jobs as $job)
		{
			$retailer_id=Posting::where('job_id',$id)->pluck('user_id')->first();
			$job['retailer']=User::where('id',$retailer_id)->first();
			$shop_id=Posting::where('job_id',$id)->pluck('shop_id')->first();
			$job['shop']=Shop::where('id',$shop_id)->first();
			$job['shop']['department'] = Department::where('id',$job['shop']->department)->pluck('department')->first();
			if(count(Image::where('shop_id',$shop_id)->pluck('url'))>0)
			{
				$job['shop']['images']=Image::where('shop_id',$shop_id)->pluck('url');
			}
			else
			{

			}
		}
		$job['title'] = Jobtitle::where('id',$job->title)->pluck('job_title')->first();

		return $jobs;
	}


	public function add(Request $request)
	{

		$job = new Job;	


		$job->title = $request['title'];
		$job->requirement = $request['requirement'];
		$job->salary_basis = $request['salary_basis'];
		$job->filled = 0;
		$job->save();

		$posting = new Posting;
		$posting->user_id = $request['rId'];	
		$posting->shop_id = $request['shopId'];	
		$posting->job_id = $job->id;	
		$posting->status = 'open';
		$posting->save();


		return 'Job entered';
	}

		// public function put(Request $request, $id)
		// {

		// 	Job::where('id',$id)->update($request->all());	

		// 	return "Job Updated";
		// }


	public function remove($id)
	{
		$job = Job::find($id);

		$job->delete();

		return "Job deleted";
	}


//other methods 

	public function shopJobs($sId)
	{

		$jobs = Job::whereIn('id',Posting::where([['shop_id','=',$sId],['job_id','>',0]])->select('job_id')->get())->get();

		foreach ($jobs as $j) {
			$j['title'] = Jobtitle::where('id',$j->title)->pluck('job_title')->first();
		}

		if(count($jobs) >0)
		{
			return $jobs;
		}
		else
		{
			return "noJobs";
		}

	}

	public function update(Request $request)
	{
		Job::where('id',$request['id'])->update([
			'title' => $request['title'],
			'requirement' => $request['requirement'],
			'salary_basis' => $request['salary_basis']
			]);
		return "Job Updated";

	}

	public function statusUpdate(Request $request)
	{
		$id=$request['jId'];
		$status=$request['status'];

		Job::where('id',$id)->update(['status'=>$status]);

		return "Job Status Updated";
	}


	public function all_newjobs($uId)
	{
		$applied_jobs=Applicant::where('user_id',$uId)->pluck('job_id');

		$all_other_jobs=Job::whereNotIn('id',$applied_jobs)->pluck('id');

		$departments=Experience::where('user_id',$uId)->distinct()->pluck('department');

		$matching;
		$not_matching;

		foreach($departments as $department)
		{
			$shops=Shop::where('department',$department)->pluck('id');

			$matching_jobs=Posting::where('job_id','!=','0')->whereIn('shop_id',$shops)->whereIn('job_id',$all_other_jobs)->pluck('job_id');
			$matching=$matching_jobs;

			$not_matching_jobs=Job::whereNotIn('id',$applied_jobs)->whereNotIn('id',$matching_jobs)->pluck('id');
			$not_matching=$not_matching_jobs;

		}

			// $final['matching']=$matching;
			// $final['other']=$not_matching;
		$result=[];
		$jobs=Job::whereIn('id',$matching)->get();
		for ($i=0; $i <count($jobs) ; $i++) 
		{ 

			$jobs_shopid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('shop_id');
			$jobs_retailerid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('user_id');
			$jobs[$i]['retailer']=User::where('id',$jobs_retailerid)->select('id','type',
				'name','email','gender','dob','describe_yourself','salary_basis','mobile','address')->first();
			$jobs[$i]['shop']=Shop::where('id',$jobs_shopid)->select('id','name','contact','address','working_hours','established_year','department')->first();
			$jobs[$i]['shop']['department'] = Department::where('id',$jobs[$i]['shop']['department'])->pluck('department')->first();

			$jobs[$i]['title'] = Jobtitle::where('id',$jobs[$i]->title)->pluck('job_title')->first();

			$image_count=count(Image::where('shop_id',$jobs_shopid)->pluck('url'));
			
			if($image_count>0)
			{
				$jobs[$i]['shop']['images']=Image::where('shop_id',$jobs_shopid)->pluck('url');
			}

			
		}
		$result['matched'] =  $jobs;

		$jobs=Job::whereIn('id',$not_matching)->get();
		for ($i=0; $i <count($jobs) ; $i++) 
		{ 

			$jobs_shopid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('shop_id');
			$jobs_retailerid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('user_id');
			$jobs[$i]['retailer']=User::where('id',$jobs_retailerid)->select('id','type',
				'name','email','gender','dob','describe_yourself','salary_basis','mobile','address')->first();
			$jobs[$i]['shop']=Shop::where('id',$jobs_shopid)->select('id','name','contact','address','working_hours','established_year','department')->first();
			$jobs[$i]['shop']['department'] = Department::where('id',$jobs[$i]['shop']['department'])->pluck('department')->first();

			$jobs[$i]['title'] = Jobtitle::where('id',$jobs[$i]->title)->pluck('job_title')->first();

			$image_count=count(Image::where('shop_id',$jobs_shopid)->pluck('url'));
			
			if($image_count>0)
			{
				$jobs[$i]['shop']['images']=Image::where('shop_id',$jobs_shopid)->pluck('url');
			}

			
		}
		$result['not_matched'] =  $jobs;
		return $result;

	}


}
