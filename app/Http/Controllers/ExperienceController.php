<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Experience;
use App\Department;
class ExperienceController extends Controller
{
    public function create(Request $request)
    {


        $from_year=(int)$request['fromyear'];
        $to_year=(int)$request['toyear'];
        $years =$to_year-$from_year;
        
        $exp = new Experience;
        $exp->user_id=$request['sId'];
        $exp->from_year=$request['fromyear'];
        $exp->to_year=$request['toyear'];
        
        $exp->years=$years;
        $exp->department=$request['department'];
        $exp->designation=$request['designation'];
        $exp->company=$request['company'];
        $exp->address=$request['address'];
        $exp->pincode=$request['pincode'];
        $exp->salary=$request['salary'];


        $exp->save();
        
        

        return "Experience Added Successfully";
    }

    public function salesman_view($sId)
    {

        $salesman_exp=Experience::where('user_id',$sId)->get();
        if(count($salesman_exp)>0)
        {
            foreach ($salesman_exp as $salesman) {
              $salesman['department']=Department::where('id',$salesman['department'])->pluck('department')->first();
          }
          return $salesman_exp ;
      }
      else
      {
        return "noExperince";
    }
}
public function view($eId)
{

    $experince=Experience::where('id',$eId)->get();

    $experince[0]['department'] = Department::where('id',$experince[0]->department)->pluck('department')->first();
    
    return $experince;
    
}

public function update(Request $request)
{
    
    
    Experience::where('id',$request['eId'])->update([
        'years'=>$request['years'],
        'department'=>$request['department'],
        'designation'=>$request['designation'],
        'company'=>$request['company'],
        'address'=>$request['address'],
        'pincode'=>$request['pincode'],
        'salary'=>$request['salary'],
        'from_year'=>$request['fromyear'],
        'to_year'=>$request['toyear']
        ]);

    
    return "Experience Updated Successfully";
}

public function delete($uId)
{
    Experience::where('user_id',$uId)->delete();
    return "Deleted Successfully";
}
}