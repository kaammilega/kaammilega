<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobtitle extends Model {


	protected $table='job_titles';
	public $timestamps =false;
	protected $guarded = [];

	protected $dates = [];

	public static $rules = [
        // Validation rules
	];

}
