function barload()
{
    var ctx = document.getElementById("mycanvas");
    var barChartData=
        {
            labels:["may","june","july","august","september"],
            datasets:[
                    {
                        label: "No of Jobs",
                        data:[10,50,40,20,100],
                        backgroundColor:"rgba(32,178,170, 0.5)",
                        borderColor:"rgba(32,178,170,0.5)",
                        hoverbackgroundColor:"rgba(255, 206, 86,1)",
                        hoverborderColor:"rgba(255, 206, 86,1)",
                        borderWidth: 1

                    },
                    
                    {
                        label: "No of Applicants",
                        data:[50,30,10,70,90],
                        backgroundColor:"rgba(255, 159, 64, 0.4)",
                        borderColor:"rgba(255, 159, 64, 0.5)",
                        hoverbackgroundColor:"rgba(255, 159, 64, 1)",
                        hoverborderColor:"rgba(255, 159, 64, 1)",
                        borderWidth: 1

                    },
                    {
                        type:'line',
                        label: "No of placements",
                        data:[30,20,40,60,80],
                        fill:false,
                        lineTension:0.1,
                        backgroundColor:"rgba(153, 102, 255, 0.75)",
                        borderColor:"rgba(153, 102, 255,1)",
                        pointHoverBackgroundColor:"rgba(153, 102, 255, 1)",
                        pointHoverBorderColor:"rgba(255, 206, 86,1)",
                        

                    }
                    ]
        
        };

        // var options = {
        //     title:{
        //         display:true,
        //         position:"top",
        //         text:"Mixed Graph",
        //         fontSize:18,
        //         fontColor:"#111"

        //     },
        //     legend:{
        //         display:true,
        //         position:"bottom"
        //     }
        // };
    
    var barChart = new Chart(ctx,
    {
        type:'bar',
        data:barChartData,
        option:{}
    });
 
 var ctp = document.getElementById("mypie");
 var pieChartData=
        {
            labels:["may","june","july"],
            datasets:[
                    {
                        label: "No of Jobs",
                        data:[10,50,40],
                        backgroundColor:[
                        "rgba(32,178,170, 0.5)",
                        "rgba(255, 159, 64, 0.4)",
                        "rgba(153, 102, 255, 0.75)"
                        ],
                        borderColor:[
                        "rgba(32,178,170,0.5)",
                        "rgba(255, 159, 64, 0.4)",
                        "rgba(153, 102, 255, 0.75)"
                        ],
                       
                        borderWidth: [1,1,1]

                    }
                    ]
                };


 var pieChart = new Chart(ctp,
    {
        type:'pie',
        data:pieChartData,
        option:{}
    });
   
    
}



   