	$(document).ready(function()
	{
		ajax_department();
		datatable_department();
		$('.ui-pnotify').remove();
		
	});
	var baseUrl="http://ec2-13-126-220-182.ap-south-1.compute.amazonaws.com/";
	// var baseUrl="http://localhost:8000/";
//var baseUrl="http://ec2-52-14-198-231.us-east-2.compute.amazonaws.com/";




	function ajax_department()
	{
		$.ajax({
			type:"GET",
			url:baseUrl+"api/all-department",
			success:function(data)
			{
				create_selectoption(data);
				autocomplete_data(data);

			},

			error:function(err)
			{
				console.log("ERROR");
			}
		});
	}
	function datatable_department()
	{
		$('#example1').DataTable( {
			retrieve: true,
			ajax:
			{
				url:baseUrl+"datatable_department",
				type:"GET"
			},
			columns: [
			{ data:"department"}

			]
		} );

	}

	function autocomplete_data(data)
	{
		var auto_data=[];
		for(var i=0;i<data.length;i++)
		{
			var temp=data[i].department;
			auto_data[i]=temp;

		}
		
		$( ".autocom" ).autocomplete({
			source: auto_data
		});
	}
	function create_selectoption(data)
	{
		$('#department').find('option').remove().end();
		$.each(data,function(i,value){
			// console.log(value.department);
			var op = $("<option></option>").text(value.department);
			op.attr("value",value.department);
			$('#department').append(op);
		});

	}

	function data_reload()
	{
		var table= $('#example1').DataTable( {
			retrieve: true,
			ajax:
			{
				url:baseUrl+"datatable_department",
				type:"GET"
			},
			columns: [
			{ data:"department"}

			]
		} );
		table.ajax.reload();

		ajax_department();
	}

	$("#add").on("click",function()
	{

		var department_data = $("#addin").val();
		$.ajax({
			type:"POST",
			url:baseUrl+"api/add-department",
			data: {
				depart : department_data,
			},

			success:function(data)
			{
				data_reload();
				$("#addin").val("");
				console.log("Success");

				console.log(data);
				if(data == "Department_yes")
				{
					new PNotify({
						title: 'Checked!',
						text: department_data+' already in department table',
						type: 'info'
					});
					
				}
				else if(data == "Department_blank")
				{
					new PNotify({
						title: 'Blank!',
						text: 'Blank Entery not Allowed',
						type: 'info'
					});

				}
				else
				{
					
					new PNotify({
						title: 'Success!',
						text: department_data+' added to department table',
						type: 'success'
					});
				}




			},

			error:function(err)
			{
				console.log("ERROR");
			}
		});
	});


	$("#del").on("click",function()
	{

		var department_data = $("#department").val();
		$.ajax({
			type:"POST",
			url:baseUrl+"api/remove-department",
			data: {
				depart : department_data,
			},

			success:function(data)
			{
				data_reload();
				console.log("delete Success");
				new PNotify({
					title: 'Success!',
					text: department_data+' deleted from department table',
					type: 'success'
				});				
			},

			error:function(err)
			{
				console.log("ERROR");
			}
		});
	});

	$("#upd").on("click",function()
	{
		var old_data = $("#department").val();
		var new_data = $("#updata").val();
		$.ajax({
			type:"POST",
			url:baseUrl+"api/rename-department",
			data: {
				
				old : old_data,

				new : new_data,

			},

			success:function(data)
			{
				
				data_reload();
				$("#addin").val("");
				$("#updata").val("");
				console.log("update Success");
				if(data == "Department_already")
				{
					new PNotify({
						title: 'Error!',
						text: new_data+'already in department table',
						type: 'error'
					});
					
				}
				else if(data == "Department_blank")
				{
					new PNotify({
						title: 'Blank!',
						text: 'Blank Entery not Allowed',
						type: 'info'
					});
				}

				else
				{
					
					new PNotify({
						title: 'Success!',
						text: new_data+' added to department table',
						type: 'success'
					});
					
				}

			},

			error:function(err)
			{
				console.log("ERROR");
			}
		});
	});
	$("#logout").on("click",function()
	{
		sessionStorage.setItem("checkk","invalid");
		$(location).attr('href','admin');

	});