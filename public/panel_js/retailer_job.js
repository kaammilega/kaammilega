$(document).ready(function(){
	console.log('page ready3');
	ajax_call(1);
});
var pageNumber = 1;
//var baseUrl = "http://localhost:8000/";
var baseUrl="http://ec2-13-126-220-182.ap-south-1.compute.amazonaws.com/";

var totalRecords;
var recordsToShow = 10;
var result_table = [];
var first_call = true;
var table;

function ajax_call(pg)
{
	$.ajax({
		type:"POST",
		url:baseUrl+"allretailerjobs",
		data:{
			pageNumber:pg,
			recordsToShow:recordsToShow 
		},
		success:function(data)
		{
			console.log(data);
			totalRecords = data.recordsTotal;
			console.log(totalRecords);
			console.log(result_table.length);
			if(result_table.length < totalRecords){
				if(first_call == true){
					document.getElementById('page').textContent = pageNumber;
					document.getElementById('totalPages').textContent = Math.ceil(totalRecords/10);
					result_table = data.data;
					console.log('inside first call')
					makeData();
				}
				else{
					document.getElementById('page').textContent = pageNumber;
					var newData = data.data;
					newData.splice(0,1);
					result_table = result_table.concat(newData);
					console.log(result_table);
					console.log("result length"+result_table.length);
					table.destroy();
					makeData();
				}
			}
		},
		error:function(err)
		{
			console.log("ERROR");
		}
	});
}
function makeData()
{
	table = $('#retailerjobs_table').DataTable( {
		bInfo: false,
		pagingType: "simple",
		stateSave: true,
		searching:false,
		/*
		retrieve: true,
		iDisplayLength: 10,
		serverSide: true,
		
		*/
		// "order": [],
		dom: "Bfrtip",
		"aaSorting": [],
		data: result_table,
		columns: [
		{ title: "Name" },
		{ title: "Gender" },
		{ title: "Mobile" },
		{ title: "Jobs Title" }
		
		],
		drawCallback: function(){
			$('.paginate_button.next', this.api().table().container())          
			.on('click', function(){

				console.log(table.page.info());
				//pageNumber++;
				first_call = false;
				console.log(first_call);
				console.log(pageNumber);
				var pages = Math.ceil(totalRecords/10);
				console.log(pages);
				if(result_table.length < totalRecords)
					{	
						if(pageNumber<pages)
						{
							pageNumber++;
							ajax_call(pageNumber);
						}
						else if(pageNumber=pages)
						{
							ajax_call(pageNumber);
							document.getElementById('page').textContent = pageNumber;
						}
						}
						else if(result_table.length = totalRecords)
						{
							if(pageNumber<pages)
						{
							pageNumber++;
							document.getElementById('page').textContent = pageNumber;
						}
						else if(pageNumber=pages)
						{
							// ajax_call(pageNumber);
							document.getElementById('page').textContent = pageNumber;
						}

						}
						

				console.log("result length next"+result_table.length);
			});
			$('.paginate_button.previous', this.api().table().container())          
			.on('click', function(){
				// console.log(pageNumber - 1);
				console.log("result length previous"+result_table.length);
				pageNumber--;
				if(pageNumber>=1)
				{
					
				document.getElementById('page').textContent = pageNumber;
			}
			else

			{
				pageNumber=1;
				document.getElementById('page').textContent = pageNumber;

			}

			}); 
			$('.paginate_button.first', this.api().table().container())          
			.on('click', function(){
				alert("hello");
					pageNumber=1;
				document.getElementById('page').textContent = pageNumber;
			});      
		}
	} );
}

// $('#retailerjobs_table').on('page.dt', function(){
// 	console.log(table.page.info());
// 	pageNumber++;
// 	first_call = false;
// 	console.log(first_call);
// 	console.log(pageNumber);
// 	var pages = Math.ceil(totalRecords/10);
// 	console.log(pages);
// 	if(pageNumber <= pages)
// 	{ajax_call(pageNumber);}
// });

$("#logout").on("click",function()
{
	sessionStorage.setItem("checkk","invalid");
	$(location).attr('href','admin');

});